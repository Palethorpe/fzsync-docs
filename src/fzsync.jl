module fzsync

using GRUtils, IJulia, CSV, DataFrames

export plot_diffs, weave_a_rare_data_race

function plot_diffs()
    csv = CSV.read("/home/rich/qa/timings.csv")
    csv.i = [1:nrow(csv)...]
    diffs = [csv.a_start .- csv.b_start csv.a_end .- csv.b_end]
    A_wins = filter(r -> r.winner == "A", csv)
    winning_diffs = [A_wins.a_start .- A_wins.b_start A_wins.a_end .- A_wins.b_end]
    gcf(Figure((800, 600)))
    plot(diffs[:, 1], ".k")
    ylabel("Difference (ns)")
    title("Start A - Start B")
    ylim(-10000, 70000)
    yticks(10000, 1)
    oplot(A_wins.i, winning_diffs[:, 1], "or")
    savefig("start_difference.png")
    plot(diffs[:, 2], ".k")
    ylabel("Difference (ns)")
    title("End A - End B")
    oplot(A_wins.i, winning_diffs[:, 2], "or")
    ylim(-70000, 20000)
    yticks(10000, 1)
    savefig("end_difference.png")
end

weave_a_rare_data_race() = weave("src/a_rare_data_race.pmd";
                                 doctype="pandoc2html",
                                 pandoc_options=["--metadata-file=src/a_rare_data_race.yml", "--toc"],
                                 out_path=:pwd)

end # module
